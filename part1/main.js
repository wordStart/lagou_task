/*
  1. JS异步编程
    因为JS是单线程代码，所以当同步执行太多就会堵塞，造成卡顿，所以需要使用异步编程，把需要时间的代码执行放到异步，不影响代码的执行
  2. EventLoop
    EventLoop 是一种循环机制，不断的去查找队列中需要执行的事件，并执行
  3. 消息队列
    消息队列是用来存放宏任务的队列，比如定时器与ajax里的回调，当时间到了之后，会把回调放入消息队列中
  4. 宏任务
    宏任务是执行完之后  不需要立刻执行的代码，比如settimeout，事件
  5. 微任务
    微任务是当前任务执行完需要立即执行的任务，比如promise.then
*/

/* 代码题 */
const fp = require('lodash/fp')

{
  const promise = new Promise((resolve, reject) => {
    resolve('hello')
  })
  promise.then(val => val + 'lagou').then(val => console.log(val + 'I ❤ U'))
}

{
  // 数据
  // horsepower 马力,dollar_value 价格 in_stock 库存
  const cars = [
    {
      name: 'Ferrari FF', horsepower: 660,
      dollar_value: 700000, in_stock: true
    },
    {
      name: 'Spyker C12 zagato', horsepower: 650,
      dollar_value: 648000, in_stock: false
    },
    {
      name: 'Jaguar XKR-S', horsepower: 550,
      dollar_value: 132000, in_stock: false
    },
    {
      name: 'Audi R8', horsepower: 525,
      dollar_value: 114200, in_stock: false
    },
    {
      name: 'Aston Martin One-77', horsepower: 750,
      dollar_value: 1850000, in_stock: true
    },
    {
      name: 'Pagani Huayra', horsepower:700, 
      dollar_value: 1300000, in_stock: false
    }
  ]
  // 1
  const lastInStock = fp.flowRight(fp.prop('in_stock') ,fp.last)
  console.log(lastInStock(cars))
  
  // 2
  const firstInCarName = fp.flowRight(fp.prop('name') ,fp.first)
  console.log(firstInCarName(cars))
  
  //3
  let _average = function (xs) {
    return fp.reduce(fp.add, 0, xs) / xs.length
  } // <- 无须改动

  let averageDollarValue = fp.flowRight(_average, fp.map('dollar_value'))
  console.log(averageDollarValue(cars))

  // 4
  let _underscore = fp.replace(/\W+/g, '_')
  let sanitizeNames = fp.map(fp.flowRight(_underscore, fp.toLower, fp.prop('name')))
  console.log(sanitizeNames(cars))
}

{
  class Container {
    static of(value) {
      return new Container(value)
    }
    constructor(value) {
      this._value = value
    }
    map(fn) {
      return Container.of(fn(this._value))
    }
  }

  class Maybe {
    static of(x) {
      return new Maybe(x)
    }
    isNothing() {
      return this._value === null || this._value === undefined
    }
    constructor(x) {
      this._value = x
    }
    map(fn) {
      return this.isNothing() ? this : Maybe.of(fn(this._value))
    }
  }
  // 1
  let maybe = Maybe.of([5, 6, 1])
  let ex1 = maybe.map(i => fp.map(fp.add(1), i))
  console.log(ex1)

  // 2
  let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
  let ex2 = xs.map(i => fp.first(i))
  console.log(ex2)

  // 3
  let safeProp = fp.curry(function (x, o) {
    return Maybe.of(o[x])
  })
  let user = { id: 2, name: 'Albert' }
  let ex3 = safeProp('name', user).map(i => fp.first(i))
  console.log(ex3)

  // 4
  let ex4 = n => Maybe.of(n).map(parseInt)
  console.log(ex4('5'))

}

{
  /* 手写Promise */
  /* promise 调用 */
  // const promise = new Promise((resolve, reject) => {
  //   resolve('success')
  // })

  // promise.then(val => console.log(val), reason => console.log(reason))

  /*
    Promise 有三个状态，pending，fulfilled，rejected，只能修改一次
    Promise 需要一个自执行函数，resolve, reject 俩个参数
    Promise 有then方法，返回一个promise then需要支持链式调用  then的俩个参数是可选的 then中的返回值作为下一个then的初始值
    Promise.finally 无论是否成功都会执行
    Promise.all 传入一个数组，相同顺序的输入 输出相同顺序的输出
    Promise.resolve 为一个非Promise的值进行包装，拥有then方法
    Promise.catch 捕捉promise联调上的异常
    promise不能自己调用自己
  */
  const PENDING = 'pending'
  const FULFILLED = 'fulfilled'
  const REJECTED = 'rejected'
  class MyPromise {
    constructor(executor) {
      try {
        executor(this.resolve, this.reject)
      } catch (error) {
        this.reject(error)
      }
    }
    // 状态默认初始值
    status = PENDING
    // 成功的值
    value = undefined
    // 失败的原因
    reason = undefined
    successCallback = []
    failCallback = []

    resolve = (value) => {
      if (this.status !== PENDING) return
      this.status = FULFILLED
      this.value = value
      while (this.successCallback.length) this.successCallback.shift()()
    }

    reject = (reason) => {
      if (this.status !== PENDING) return
      this.status = REJECTED
      this.reason = reason
      while (this.failCallback.length) this.failCallback.shift()()
    }

    then = (successCallback, failCallback) => {
      successCallback = successCallback ? successCallback : value => value
      failCallback = failCallback ? failCallback : reason => { throw reason }

      let promise2 = new MyPromise((resolve, reject) => {
        if (this.status === FULFILLED) {
          setTimeout(() => {
            try {
              let x = successCallback(this.value)
              resolvePromise(promise2, x, resolve, reject)
            } catch (error) {
              reject(error)
            }
          }, 0);
        } else if (this.status === REJECTED) {
          setTimeout(() => {
            try {
              let x = failCallback(this.reason)
              resolvePromise(promise2, x, resolve, reject)
            } catch (error) {
              reject(error)
            }
          }, 0);
        } else {
          /* 当status状态为pending 在resolve与reject调用对应回调 */
          this.successCallback.push(() => {
            setTimeout(() => {
              try {
                let x = successCallback(this.value)
                resolvePromise(promise2, x, resolve, reject)
              } catch (error) {
                reject(error)
              }
            }, 0);
          })
          this.failCallback.push(() => {
            setTimeout(() => {
              try {
                let x = failCallback(this.reason)
                resolvePromise(promise2, x, resolve, reject)
              } catch (error) {
                reject(error)
              }
            }, 0);
          })
        }
      })
      return promise2
    }
    catch = (failCallback) => {
      return this.then(undefined, failCallback)
    }
    finally = (callback) => {
      return this.then(value => {
        return MyPromise.resolve(callback()).then(() => value)
      }, reason => {
        return MyPromise.resolve(callback()).then(() => { throw reason })
      })
    }
    static all = (array) => {
      let result = []
      let index = 0
      return new MyPromise((resolve, reject) => {
        function addOne(key, value) {
          result[key] = value
          index++
          if (index === array.length) {
            resolve(result)
          }
        }
        for (let i = 0; i < array.length; i++) {
          let current = array[i]
          if (current instanceof MyPromise) {
            current.then(value => addOne(i, value), reason => reject(reason))
          } else {
            addOne(i, current)
          }
        }
      })
    }
    static resolve = (value) => {
      if (value instanceof MyPromise) return value
      return new MyPromise(resolve => resolve(value))
    }
  }
  /* 判断返回值是不是promise 以及promise是否自身调用自身 */
  function resolvePromise(promise2, x, resolve, reject) {
    if (promise2 === x) {
      return reject(new TypeError('Chaining cycle detected for promise #<Promise>'))
    }
    if (x instanceof MyPromise) {
      x.then(resolve, reject)
    } else {
      resolve(x)
    }
  }
  const myPromise = new MyPromise((resolve, reject) => {
    resolve('success')
  })
  myPromise.then(val => {
    console.log(val)
    return 111
  }).then(val => console.log(val))
}
